import {currency} from "./url";

const headers = {
	method: "GET",
	headers: {
		'Content-Type': 'application/json',
		// 'Content-Type': 'application/x-www-form-urlencoded',
	},
};

export function getCurrencyData(currencyName) {
	return fetch(currency(currencyName), headers)
		.then((response) => {
			return response.json();
		});
}