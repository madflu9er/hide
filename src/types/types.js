export const IconPosition = {
    left: 1,
    right: 2
};

export const LoginState = {
    login: 1,
    signup: 2
};