import DashboardContent from "../components/DashboardContentComponent";
import BuyMachinesContent from "../components/BuyMachinesContentComponent";

export const routes = [
  {
    path: "/dashboard",
    component: DashboardContent
  },

  {
    path: "/buy",
    component: BuyMachinesContent
  }
];
