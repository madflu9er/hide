import React, { Component } from 'react';
import {Redirect, Route, Switch} from "react-router";
import {BrowserRouter} from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import {routes} from "./config/router";
import Header from "./components/HeaderComponent";
import MenuBar from "./components/MenuBarComponent";
import DashBoardDetails from "./components/DashboardDetailsComponent";


//mocked
const user = {
  userId: "dkf-9sd-f82-38f",
  accountName: "testmail@gmail.com",
  accountIncome: 254.23,
  accountOutCome: 25.17,
  balance: 144.12,
  machines: [
    {
      type: 0,
      name: "BMiner",
      currency: "BTC",
      count: 2,
      hashRate: 2.2847,
    },
    {
      type: 1,
      name: "EMiner",
      currency: "ETH",
      count: 1,
      hashRate: 1.8347,
    },
    {
      type: 2,
      name: "DMiner",
      currency: "DOGE",
      count: 3,
      hashRate: 1.7347,
    },
    {
      type: 3,
      name: "LMiner",
      currency: "LTC",
      count: 1,
      hashRate: 0.7347,
    }
  ]
};


class App extends Component {

  render() {

    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={LoginPage} />
          {!user ? (<Redirect to="/"/>) : (
            <div className="main-page">
              <Header
                hideName="HASH-FACTORY"
                buttonLoginText="Авторизация"
                buttonSignUpText="Регистрация"
                logged={true}
                loggedText="Settings"
                options={["Logout", "Password"]}
              />
              <div className="main-page_content">
                <MenuBar />
                <div className="dashboard">
                  <DashBoardDetails
                    accountName={user.accountName}
                    accountIncome={user.accountIncome}
                    accountOut={user.accountOutCome}
                    balance={user.balance}
                  />
                  {routes.map(({path, component: Component}) => (
                    <Route
                      path={path}
                      render={(props) => <Component {...props} user={user} /> }
                    />))
                  }
                </div>
              </div>
            </div>
          )}
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
