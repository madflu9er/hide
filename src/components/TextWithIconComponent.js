import React from "react";
import { IconPosition } from "../types/types";

const TextWithIcon =
    ({
      text,
      icon,
      iconPosition
     }) => {
    return(
      <div className={`text-with-icon-wrapper ${iconPosition === IconPosition.left ? "icon-first" : "text-first"}`}>
          <span>{text}</span>
          <img src={icon} alt=""/>
      </div>
    );
};
export default TextWithIcon;