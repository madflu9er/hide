import React from "react"

const Number = ({number}) => {
  switch (number) {
    case "1": return (
      <div className="number-wrapper">
        <div className="first-element"></div>
        <div className="second-element"></div>
        <div className="third-element-active"></div>
        <div className="fourth-element"></div>
        <div className="fives-element"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element"></div>
      </div>
    );
    case "2": return (
      <div className="number-wrapper">
        <div className="first-element-active"></div>
        <div className="second-element"></div>
        <div className="third-element-active"></div>
        <div className="fourth-element-active"></div>
        <div className="fives-element-active"></div>
        <div className="sixes-element"></div>
        <div className="seventh-element-active"></div>
      </div>
    );
    case "3": return (
      <div className="number-wrapper">
        <div className="first-element-active"></div>
        <div className="second-element"></div>
        <div className="third-element-active"></div>
        <div className="fourth-element-active"></div>
        <div className="fives-element"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element-active"></div>
      </div>
    );
    case "4": return (
      <div className="number-wrapper">
        <div className="first-element"></div>
        <div className="second-element-active"></div>
        <div className="third-element-active"></div>
        <div className="fourth-element-active"></div>
        <div className="fives-element"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element"></div>
      </div>
    );
    case "5": return (
      <div className="number-wrapper">
        <div className="first-element-active"></div>
        <div className="second-element-active"></div>
        <div className="third-element"></div>
        <div className="fourth-element-active"></div>
        <div className="fives-element"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element-active"></div>
      </div>
    );
    case "6": return (
      <div className="number-wrapper">
        <div className="first-element-active"></div>
        <div className="second-element-active"></div>
        <div className="third-element"></div>
        <div className="fourth-element-active"></div>
        <div className="fives-element-active"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element-active"></div>
      </div>
    );
    case "7": return (
      <div className="number-wrapper">
        <div className="first-element-active"></div>
        <div className="second-element"></div>
        <div className="third-element-active"></div>
        <div className="fourth-element"></div>
        <div className="fives-element"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element"></div>
      </div>
    );
    case "8": return (
      <div className="number-wrapper">
        <div className="first-element-active"></div>
        <div className="second-element-active"></div>
        <div className="third-element-active"></div>
        <div className="fourth-element-active"></div>
        <div className="fives-element-active"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element-active"></div>
      </div>
    );
    case "9": return (
      <div className="number-wrapper">
        <div className="first-element-active"></div>
        <div className="second-element-active"></div>
        <div className="third-element-active"></div>
        <div className="fourth-element-active"></div>
        <div className="fives-element"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element-active"></div>
      </div>
    );
    case "0": return (
      <div className="number-wrapper">
        <div className="first-element-active"></div>
        <div className="second-element-active"></div>
        <div className="third-element-active"></div>
        <div className="fourth-element"></div>
        <div className="fives-element-active"></div>
        <div className="sixes-element-active"></div>
        <div className="seventh-element-active"></div>
      </div>
    );
    case ".": return (
      <div className="number-wrapper">
        <div className="dot"></div>
      </div>
    );
    default: return (
      <div className="number-wrapper">
        <div className="first-element"></div>
        <div className="second-element"></div>
        <div className="third-element"></div>
        <div className="fourth-element"></div>
        <div className="fives-element"></div>
        <div className="sixes-element"></div>
        <div className="seventh-element"></div>
      </div>
    )
  }
};

export default Number
