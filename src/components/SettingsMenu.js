import React from "react";

const SettingsMenu = (
  {
    isOpened,
    options
  }) => {

  return(
    <div className={isOpened ? "settings_menu_opened" : "settings_menu_closed"}>
      {options.map((item, index) => (
        <ul tabIndex={index}>
          <li>{item}</li>
        </ul>))}
    </div>
  );

}

export default SettingsMenu;
