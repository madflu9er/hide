import React from  "react";
import MenuButton from "./MenuButtonComponent";
import {Link} from "react-router-dom";

const MenuBar = () => {
    return(
        <div className="menu-bar">
          <Link to="/dashboard">
            <MenuButton name="DASHBOARD"/>
          </Link>
          <Link to="/buy">
            <MenuButton name="BUY MACHINES" />
          </Link>
          <Link to="/withdraw">
            <MenuButton name="WITHDRAW CASH" />
          </Link>
          <Link to="/swap-currency">
            <MenuButton name="SWAP CURRENCY" />
          </Link>
          <Link to="/add-founds">
            <MenuButton name="ADD FOUNDS" />
          </Link>
          <Link to="/about">
            <MenuButton name="ABOUT" />
          </Link>
          <Link to="/rules">
            <MenuButton name="RULES" />
          </Link>
        </div>
    )
}
export default MenuBar;
