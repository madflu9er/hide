import React, {useEffect, useState} from "react";
import Miner from "./MinerComponent";
import c3 from "c3";
import {getCurrencyData} from "../api/currencyApi";

const DashboardContent = ({user}) => {

	let graphicData = {
		bindto: "#chart",
		data: {
			x: 'x',
			columns: [
				["x"],
				["USD"]
			],
			colors: {
				USD: '#164573',
			},
			type: "area-spline",
		},
		zoom: {
			enabled: true
		},
		axis: {
			x: {
				type: 'category'
			}
		}
	};
  const [currencyName, setCurrencyName] = useState("BTC");

	useEffect(() => {
		getCurrencyData(currencyName).then((response) => {
			response.Data.forEach(item => {
				let date = new Date(item.time * 1000);
				let timeStr = `${date.getDate()}`;
				graphicData.data.columns[0].push(timeStr);
				graphicData.data.columns[1].push(item.close);
			})
		}).then(() => {
			let chart = c3.generate(graphicData);
		});
	});

	return (
		<div className="content-board">
			<div className="miner-stand">
				{user.machines.map(machine => (
					<Miner miner={machine} setCurrency={setCurrencyName}/>
				))}
			</div>
      <div className="chart-stand">
        <div>{currencyName}</div>
        <div id="chart"/>
      </div>

		</div>
	)
}
export default DashboardContent;
