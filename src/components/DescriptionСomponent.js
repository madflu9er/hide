import React from "react"
import bank from "../icons/bank.svg";
import bankborder from "../icons/bankborder.svg";
import bankline from "../icons/bankline.svg";
import halfline from "../icons/halfline.svg";
import machines from "../icons/machines.svg";
import machinesline from "../icons/machinesline.svg";
import money from "../icons/money.svg";
import processor from "../icons/processor.svg";

const Description = () => {
  return(
    <div className="description">
      <div className="processor-wrapper">
        <img src={processor} alt=""/>
      </div>
      <div className="machines-wrapper">
        <img src={machines} alt=""/>
      </div>
      <div className="machinesline-wrapper">
        <img src={machinesline} alt=""/>
      </div>
      <div className="bankline-wrapper">
        <img src={halfline} alt=""/>
      </div>
      <div className="bankline-2-wrapper">
        <img src={halfline} alt=""/>
      </div>
      <div className="bank-border-wrapper">
        <img src={bankborder} alt=""/>
      </div>
      <div className="bank-wrapper">
        <img src={bank} alt=""/>
      </div>
      <div className="bank-line-wrapper">
        <img src={bankline} alt=""/>
      </div>
      <div className="money-wrapper">
        <img src={money} alt=""/>
      </div>
      <div className="teg_1_text">1. Орендуем<br/>оборудование </div>
      <div className="teg_2_text">2. Добываем<br/>монеты </div>
      <div className="teg_3_text">3. Продаем<br/>на биржах </div>
      <div className="teg_4_text">4. Выплачиваем<br/>процент </div>
    </div>
  )
}

export default Description;
