import React from "react";
import TextWithIcon from "./TextWithIconComponent";
import account from "../icons/account.svg";
import upArrow from "../icons/uparrow.svg"
import downArrow from "../icons/downarrow.svg"
import { IconPosition } from "../types/types";

const DashBoardDetails =
    ({
        accountName,
        accountIncome,
        accountOut,
        balance
    }) => {
    return(
        <div className="dashboard-details">
            <TextWithIcon text={accountName} icon={account} iconPosition={IconPosition.left} />
                <TextWithIcon text={`$${accountIncome}`} icon={upArrow} iconPosition={IconPosition.right}/>
                <TextWithIcon text={`$${accountOut}`} icon={downArrow} iconPosition={IconPosition.right}/>
                <TextWithIcon text={`BALANCE: $${balance}`} />
        </div>
    )
};
export default DashBoardDetails;
