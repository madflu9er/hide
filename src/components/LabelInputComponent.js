import React from "react";

const LabelInput =
  ({
    labelText,
    inputPlaceholder,
    onInputChangeFunction,
  }) => {
  return (
    <div className="label-input_wrapper">
      <label>{labelText}</label>
      <input type="text" placeholder={inputPlaceholder}/>
    </div>
  )
}

export default LabelInput;

