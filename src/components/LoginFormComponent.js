import React from "react";
import LabelInput from "../components/LabelInputComponent"
import SubmitButton from "../components/SubmitButtonComponent";
import {LoginState} from "../types/types";

const LoginForm =
  ({
     authState
  }) => {
  return(
    <div className="login_form">
      <div className="login_form_shape">
        <LabelInput  labelText="Email" inputPlaceholder="Введите адресс електронной почты"/>
        <LabelInput  labelText="Пароль" inputPlaceholder="Введите пароль"/>
        {authState === LoginState.signup && (
          <LabelInput  labelText="Повторите пароль" inputPlaceholder="введите пароль еще раз"/>
        )}
        {authState === LoginState.signup ? (
          <SubmitButton
            name="Зарегистрироваться"
          />
        ) : (
          <SubmitButton
            name="Войти"
          />
        )}
      </div>
    </div>
  );
};

export default LoginForm
