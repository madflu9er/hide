import React from "react";

const MenuButton =
    ({
     name
    }) => {
    return (
        <button
            className="menu-button">
             {name}
        </button>
    );
}

export default MenuButton;