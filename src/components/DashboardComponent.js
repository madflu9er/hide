import React from "react"
import DashBoardDetails from "./DashboardDetailsComponent";
import Content from "./DashboardContentComponent";

const Dashboard =
    ({
      accountName,
      accountIncome,
      accountOut,
      balance,
      contentComponent
    }) => {
    return(
        <div className="dashboard">
            <DashBoardDetails
                accountName={accountName}
                accountIncome={accountIncome}
                accountOut={accountOut}
                balance={balance}
            />
          {contentComponent}
        </div>
    )
};

export default Dashboard;
