import React from "react";

const SubmitButton = (
  {
    action,
    name
  }) => {
  return(
    <div className="submit_button_wrapper">
      <input type="submit" onClick={action} value={name}/>
    </div>
  );
}
export default SubmitButton;
