import React from "react";
import settings from "../icons/settings.svg";
import SettingsMenu from "./SettingsMenu";
import { LoginState } from "../types/types";

const Header = (
  {
    hideName,
    authState,
    changeAuthState,
    buttonLoginText,
    buttonSignUpText,
    logged,
    loggedText,
    options
  }) => {

  const [isMenuOpened, setIsMenuOpened] = React.useState(false);

  const toggleMenu = () => {
    if(isMenuOpened) {
      setIsMenuOpened(false);
    }
    else setIsMenuOpened(true);
  };

  const onBlurToggle = () => {
    if(isMenuOpened) {
      setIsMenuOpened(false);
    }
  };

  const switchStateToLogin = () => {
    if(authState === LoginState.signup){
      changeAuthState(LoginState.login);
    }
  };

  const switchStateToSignUp = () => {
    if(authState === LoginState.login){
      changeAuthState(LoginState.signup);
    }
  };

  return(
    <div className="header">
      <div className="header_name">{hideName}</div>
      {!logged ? (
        <div className="header_buttons_wrapper">
          <div className="header_button-wrapper">
            <button
              className="header_button"
              onClick={() => {switchStateToLogin()}}
            >
              {buttonLoginText}
            </button>
          </div>
          <div className="header_button-wrapper">
            <button
              className="header_button"
              onClick={() => {switchStateToSignUp()}}
            >
              {buttonSignUpText}
            </button>
          </div>
        </div>
      ) : (
        <div className="header_buttons_wrapper" onBlur={() => {onBlurToggle()}} >
          <button
            className={`header_settings_button ${isMenuOpened ? "active_settings" : ""}`}
            onClick={() => {toggleMenu()}}
          >
            <div className="header_settings_text">{loggedText}</div>
            <div className="header_settings_icon_wrapper">
              <img src={settings} alt="Settings"/>
            </div>
            <SettingsMenu isOpened={isMenuOpened} options={options} />
          </button>
        </div>
      )}
    </div>
  );
};

export default Header;
