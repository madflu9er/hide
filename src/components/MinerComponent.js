import React from "react";
import ReactSpeedometer from "react-d3-speedometer"

const Miner = ({miner, setCurrency}) => {


    const currency = miner.currency;
    return(
      <div className="miner-machine" onClick={()=>{setCurrency(currency)}}>
          <div>{miner.name}</div>
          <div>{miner.count}</div>
        <div>
          <ReactSpeedometer
            width="170"
            height="150"
            minValue={0}
            maxValue={miner.hashRate+1}
            value={miner.hashRate}
            maxSegmentLabels={12}
            segments={3}
            segmentColors={[
              '#2985CC',
              '#ECEFF4',
              '#138808'
            ]}
            needleColor="#164573"
          />
        </div>
      </div>
    );
};

export default Miner;