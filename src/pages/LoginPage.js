import React  from "react";
import Header from "../components/HeaderComponent"
import Description from "../components/DescriptionСomponent";
import LoginForm from "../components/LoginFormComponent";
import { LoginState } from "../types/types";

const LoginPage = () => {

  const [authState, setAuthState] = React.useState(LoginState.login);

    return(
      <div className="login-page">
        <Header
          hideName="HASH-FACTORY"
          buttonLoginText="Авторизация"
          buttonSignUpText="Регистрация"
          logged={false}
          loggedText="Settings"
          options={["Logout", "Password"]}
          changeAuthState = {setAuthState}
          authState={authState}
        />
        <div className="content-wrapper">
          <Description/>
          <LoginForm authState = {authState} />
        </div>
      </div>
    );
};

export default LoginPage;
